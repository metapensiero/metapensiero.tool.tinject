# -*- coding: utf-8 -*-
# :Project:   metapensiero.tool.tinject — Packaging metadata
# :Created:   mer 29 giu 2022, 10:42:40
# :Author:    Lele Gaifax <lele@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: © 2022, 2023, 2024 Lele Gaifax
#

[project]
name = "metapensiero.tool.tinject"
description = "Automate creation of sources"
version = "2.0"
authors = [{ name = "Lele Gaifax", email = "lele@metapensiero.it" }]
readme = "README.rst"
license = { text = "GPL-3.0-or-later" }
classifiers = [
  "Environment :: Console",
  "Programming Language :: Python",
  "Programming Language :: Python :: 3",
  'Programming Language :: Python :: 3 :: Only',
  "Development Status :: 5 - Production/Stable",
  "Intended Audience :: Developers",
  "License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)",
  "Topic :: Software Development",
  "Topic :: Utilities",
]
keywords = [
  "YAML",
  "Jinja2",
  "scaffolding",
  "skeleton",
]
requires-python = ">=3.9"
dependencies = [
  "jinja2",
  "jinja2-time",
  "questionary",
  "ruamel.yaml",
]

[project.urls]
Changelog = "https://gitlab.com/metapensiero/metapensiero.tool.tinject/-/blob/master/CHANGES.rst"
Source = "https://gitlab.com/metapensiero/metapensiero.tool.tinject"

[project.optional-dependencies]
dev = [
  "build",
  "bump-my-version",
  "tomli",
  "twine",
]

[project.scripts]
tinject = "metapensiero.tool.tinject.__main__:main"

[build-system]
requires = ["pdm-backend"]
build-backend = "pdm.backend"

[tool.pdm.build]
package-dir = "src"
includes = [ "src/metapensiero" ]
source-includes = [
  "flake.*",
  "*.rst",
  "Justfile",
  "examples/",
]
excludes = [ "doc/_build" ]

[tool.bumpversion]
current_version = "2.0"
parse = "(?P<major>\\d+)\\.(?P<minor>\\d+)(?:\\.(?P<pre_label>[a-zA-Z-]+)(?P<pre_number>0|[1-9]\\d*))?"
serialize = [
    "{major}.{minor}.{pre_label}{pre_number}",
    "{major}.{minor}",
]
search = "{current_version}"
replace = "{new_version}"
regex = false
ignore_missing_version = false
ignore_missing_files = false
tag = false
sign_tags = false
allow_dirty = false
commit = false

[tool.bumpversion.parts.pre_label]
values = ["dev", "final"]
optional_value = "final"

[[tool.bumpversion.files]]
filename = "pyproject.toml"
search = 'version = "{current_version}"'
replace = 'version = "{new_version}"'
